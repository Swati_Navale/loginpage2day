package com.example.loginpage2day;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public EditText username,password;
    public Button submit,register;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDb = new DatabaseHelper(this);
        username = (EditText)findViewById(R.id.loginUname);
        password = (EditText)findViewById(R.id.loginPass);
        submit = (Button)findViewById(R.id.loginBtn);
        register = (Button)findViewById(R.id.newSignupBtn);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uname = username.getText().toString();
                String pass = password.getText().toString();
                Boolean chkuserpassword = myDb.chkUsernamePassword(uname,pass);
                if(chkuserpassword==true) {
                    Toast.makeText(getApplicationContext(), "Successfully Login", Toast.LENGTH_SHORT).show();
                    OpenNextActivity();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Wrong Username and Password", Toast.LENGTH_SHORT).show();
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    openSignupActivity();
            }
        });
    }

    public void OpenNextActivity() {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    public void openSignupActivity() {

        Intent intent = new Intent(this,SignupActivity.class);
        startActivity(intent);
    }
}
