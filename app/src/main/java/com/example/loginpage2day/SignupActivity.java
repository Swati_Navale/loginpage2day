package com.example.loginpage2day;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    public EditText uname,pass,active;
    public Button registerbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        myDb = new DatabaseHelper(this);
        uname = (EditText)findViewById(R.id.signupUname);
        pass = (EditText)findViewById(R.id.signupPass);
        active = (EditText)findViewById(R.id.signupActive);
        registerbtn = (Button)findViewById(R.id.signupBtn);

        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInserted = myDb.insertData(uname.getText().toString(),
                        pass.getText().toString(),
                        active.getText().toString());

                if(isInserted==true) {
                    Toast.makeText(SignupActivity.this, "Data Inserted Successfully", Toast.LENGTH_LONG).show();
                    OpenLoginPage();
                }
                else
                    Toast.makeText(SignupActivity.this,"Data Not Inserted",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void OpenLoginPage() {

        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
